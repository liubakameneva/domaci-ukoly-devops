# Postup:

## 1	Nainstalovat balicek netdata
**sudo apt install netdata**
## 2	Vytvorit [monitoring.local.config](monitoring.local.conf) v ect/apache2/sites-available
**sudo nano monitoring.local.conf**
## 3	Aktivovat stranku 
**sudo a2ensite monitoring.local.conf**<br>
Monitoring.local.config se objevil v sites-enabled
## 4   Overit spravnost a prenacist konfiguraci webserveru
**sudo apache2ctl configtest**<br>
**sudo systemctl reload apache2**
## 5	Pridat radek do souboru C:\Windows\System32\drivers\etc\hosts 
**127.0.0.1 monitoring.local**
## 6	Otevrit stranku [http://monitoring.local:8080](http://monitoring.local:8080)
![Git fork](/_screenshots/DU6/func_page.png)


