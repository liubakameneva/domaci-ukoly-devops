# Práce se soubory v Linuxu (1/3)
## 1a)
btmp.1     journal             user.log.1  auth.log.1        dpkg.log  syslog.1      daemon.log<br>
runit      private             debug.1     alternatives.log  debug     daemon.log.1  auth.log<br>
faillog    alternatives.log.1  messages.1  user.log          btmp      messages      wtmp<br>
installer  dpkg.log.1          kern.log.1  kern.log          apt       syslog        lastlog<br>
![Git fork](_screenshots/DU1/1a.JPG)

## 1b)
40K     ./apt<br>
49M     ./journal/3f796edfa995431c82e238bf2f3425bc<br>
49M     ./journal<br>
du: adresář './private' nelze číst: Operace zamítnuta<br>
4,0K    ./private<br>
14M     ./installer/cdebconf<br>
14M     ./installer<br>
4,0K    ./runit/ssh<br>
8,0K    ./runit<br>
63M     .<br>
![Git fork](_screenshots/DU1/1b.JPG)

## 1c)
soubor patri **root adm**<br>
Jako uzivatel devops nemam pristup k souboru **cat: syslog: Operace zamítnuta**<br>
![Git fork](_screenshots/DU1/1c.JPG)

### dotaz:
Co znamena uzivatel devops? Devops je skupina uzivatelu? nebo slozka z pravem?

# Práce se soubory v Linuxu (2/3)
## 2a)
.  ..  devops  zoo<br>
![Git fork](_screenshots/DU1/2a.JPG)<br>
## 2b)
![Git fork](_screenshots/DU1/2b_1.JPG)
![Git fork](_screenshots/DU1/2b_2.JPG)
## 2c)
-rw-r----- 1 root root 0  3. říj 11.14 reditel.txt<br>
![Git fork](_screenshots/DU1/2c.JPG)
## 2d)
-rw-r--r-- 1 root sudo 0  3. říj 11.14 tygr.txt<br>
![Git fork](_screenshots/DU1/2d.JPG)
## 3a)
2022-09-19 15:13:12 install openssh-client:amd64 <none> 1:8.4p1-5+deb11u1<br>
![Git fork](_screenshots/DU1/3a.JPG)

## 3b)
2022-09-19 13:11:14 startup archives install<br>
2022-09-19 13:11:14 install base-passwd:amd64 <none> 3.5.51<br>
2022-09-19 13:11:14 status half-installed base-passwd:amd64 3.5.51<br>
2022-09-19 13:11:14 status unpacked base-passwd:amd64 3.5.51<br>
2022-09-19 13:11:14 configure base-passwd:amd64 3.5.51 3.5.51<br>
2022-09-19 13:11:14 status half-configured base-passwd:amd64 3.5.51<br>
2022-09-19 13:11:15 status installed base-passwd:amd64 3.5.51<br>
2022-09-19 13:11:15 startup archives install<br>
2022-09-19 13:11:15 install base-files:amd64 <none> 11.1+deb11u5<br>
2022-09-19 13:11:15 status half-installed base-files:amd64 11.1+deb11u5<br>
![Git fork](_screenshots/DU1/3b.JPG)

## 3c) 
2022-09-19 15:19:30 status half-configured man-db:amd64 2.9.4-2<br>
2022-09-19 15:19:34 status installed man-db:amd64 2.9.4-2<br>
2022-09-19 15:19:34 trigproc libc-bin:amd64 2.31-13+deb11u4 <none><br>
2022-09-19 15:19:34 status half-configured libc-bin:amd64 2.31-13+deb11u4<br>
2022-09-19 15:19:34 status installed libc-bin:amd64 2.31-13+deb11u4<br>
![Git fork](_screenshots/DU1/3c.JPG)