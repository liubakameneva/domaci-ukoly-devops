## Značky
### K čemu v Gitu slouží značky (tzv. „tagy“)?
Znacka slouzi k oznaceni konkretniho bodu v historii, ktery je pro nas dulezity.
### Jak lze značky
- vytvořit: 
Prosta znacka - znacka bez parametru: **git tag [nazev-znacky]**<br>
Anotovana znacka: **git tag -a [nazev-znacky]**<br>
Pak pro pridani do vzdaleneho repozitare: **git push origin [nazev-znacky]**<br>
Nebo pro pridani vsech vytvorenych znacek najednou: **git push origin --tags**<br>
- vypsat jejich seznam: 
**git tag**
- smazat je: 
**git tag -d "Nazev znacky"**<br>
Kdyz potrebujeme vymazat znacku ze vzdaleneho repozitare, pouzijeme prikaz: **git push origin --delete [nazev-znacky]**
### Můžete ke značce připojit i komentář nebo delší popis? Jak?
pouzitim parametru -m: **tag -a [nazev-znacky] -m "Komentar nebo popis"**

## Záchrana smazané větve
### Vymazat vetev
**git branch -d liuba**
### Vymazat vetev ze vzdaleneho repozitare
**git push origin liuba**
### Prepnout se do vymazane vetve
**git checkout -b liuba [sha]** <br>
najit sha lze pomoci prikazu git reflog nebo ve vystupu funkci branch -d
### Vratit vetev do vzdaleneho repozitare
**git push origin liuba**
